'use strict';

const _ = require('lodash');
const request = require('request-promise');

const CONNECT_TIMEOUT = 20000;
const MAX_SOCKETS = 32;

const requester = request.defaults({
  headers: {
    Accept: 'Application/json'
  },
  json: true,
  timeout: CONNECT_TIMEOUT,
  pool: {
    maxSockets: MAX_SOCKETS
  }
});

class ApiError extends Error {
  constructor(message, code) {
    super(message);
    this.code = code;
  }

  toString() {
    return `${this.constructor.name}: ${this.message} (${code})`;
  }
}

async function sendRequest(uri, qs) {
  const reqOptions = {
    uri,
    method: 'GET',
    qs
  };

  const response = await requester(reqOptions);
  if (!_.isPlainObject(response)) {
    throw new Error('Failed requesting live currency.');
  } else if (!response.success) {
    if (_.isPlainObject(response.error)) {
      throw new ApiError(response.error.info, response.error.code);
    } else {
      throw new ApiError('The API endpoints return failed result.', null);
    }
  }

  return response;
}

class CurrencyLayer {
  constructor(accessKey) {
    this._accessKey = accessKey;
  }

  async getHistoryRate(date, from = 'USD', to, { raw = false } = {}) {
    const response = await sendRequest('http://apilayer.net/api/historical', {
      access_key: this._accessKey,
      currencies: to ? _.join(to, ',') : null,
      source: from,
      date,
    });

    return raw ? response : this.getResponse(response);
  }

  async getLiveRate(from = 'USD', to, { raw = false } = {}) {
    const response = await sendRequest('http://apilayer.net/api/live', {
      access_key: this._accessKey,
      currencies: to ? _.join(to, ',') : null,
      source: from,
    });

    return raw ? response : this.getResponse(response);
  }

  formatResponse(response) {
    return {
      timestamp: response.timestamp,
      source: from,
      rates: _
        .chain(response.quotes)
        .mapValues((val, key) => ({
          currency: key.substring(3),
          rate: val,
        }))
        .values()
        .value()
    };
  }
}

CurrencyLayer.ApiError = ApiError;

module.exports = CurrencyLayer;
